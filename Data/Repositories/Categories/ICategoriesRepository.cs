﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestTask.Data.Entities;

namespace TestTask.Data.Repositories.Categories
{
    public interface ICategoriesRepository
    {
        Task<List<Category>> GetAll();

        Task<Category> GetById(int id);
    }
}