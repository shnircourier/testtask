﻿using TestTask.Business.Services.Books.Dtos;

namespace TestTask.Api.Models
{
    public class BookViewModel
    {
        public BookDto Book { get; set; }

        public BookStatisticsDto BookStatistics { get; set; }
    }
}