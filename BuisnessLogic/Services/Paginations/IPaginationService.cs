﻿namespace TestTask.Business.Services.Paginations
{
    public interface IPaginationService
    {
        int GetNextPage(int page, int pageSize);
    }
}