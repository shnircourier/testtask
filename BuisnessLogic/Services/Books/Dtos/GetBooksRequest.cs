﻿using TestTask.Business.Services.Paginations.Dtos;

namespace TestTask.Business.Services.Books.Dtos
{
    public class GetBooksRequest : IQueryParams
    {
        public int Page { get; set; } = 1;
        public string Category { get; set; }

        public string Fio { get; set; }
        
        public int? WroteAt { get; set; }
        
        public string BookName { get; set; }
    }
}