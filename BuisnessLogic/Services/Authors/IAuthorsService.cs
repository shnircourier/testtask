﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestTask.Business.Services.Authors.Dtos;
using TestTask.Business.Services.Paginations.Dtos;

namespace TestTask.Business.Services.Authors
{
    public interface IAuthorsService
    {
        Task<PaginationViewModel<AuthorDto>> GetPages(int page);

        Task<List<AuthorDto>> GetAll();
    }
}