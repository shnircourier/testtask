﻿using System.Collections.Generic;

namespace TestTask.Business.Services.Categories.Dtos
{
    public class CategoriesViewModel
    {
        public List<CategoryDto> Categories { get; set; }
    }
}