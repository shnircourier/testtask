﻿using System.Collections.Generic;
using TestTask.Business.Services.Paginations.Dtos;

namespace TestTask.Business.Services.Authors.Dtos
{
    public class AuthorsViewModel
    {
        public PageViewModel Page { get; set; }

        public List<AuthorDto> Authors { get; set; }
    }
}