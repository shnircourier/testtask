﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using TestTask.Business.Services.Authors.Dtos;
using TestTask.Business.Services.Paginations;
using TestTask.Business.Services.Paginations.Dtos;
using TestTask.Data.Repositories.Authors;

namespace TestTask.Business.Services.Authors
{
    public class AuthorsService : IAuthorsService
    {
        private readonly IPaginationService _paginationService;
        private readonly IAuthorsRepository _authorsRepository;
        private readonly IMapper _mapper;

        public AuthorsService(
            IPaginationService paginationService,
            IAuthorsRepository authorsRepository,
            IMapper mapper)
        {
            _authorsRepository = authorsRepository;
            _paginationService = paginationService;
            _mapper = mapper;
        }

        public async Task<PaginationViewModel<AuthorDto>> GetPages(int page)
        {
            const int pageSize = 12;

            var skip = _paginationService.GetNextPage(page, pageSize);
            var authors = await _authorsRepository.GetAuthors(skip, pageSize);
            var count = await _authorsRepository.GetCount();

            return new PaginationViewModel<AuthorDto>
            {
                Pagination = new PaginationDto
                {
                    Pagination = new PageViewModel(count, page, pageSize),
                },
                Items = _mapper.Map<List<AuthorDto>>(authors)
            };
        }

        public async Task<List<AuthorDto>> GetAll()
        {
            var authors = await _authorsRepository.GetAll();

            return _mapper.Map<List<AuthorDto>>(authors);
        }
    }
}