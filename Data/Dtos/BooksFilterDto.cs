﻿using System;

namespace TestTask.Data.Dtos
{
    public class BooksFilterDto
    {
        public int? Category { get; set; }

        public string Fio { get; set; }
        
        public int? WroteAt { get; set; }
        
        public string BookName { get; set; }
    }
}