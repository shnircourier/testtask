﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using TestTask.Business.Services.Books.Dtos;

namespace TestTask.Api.Models
{
    public class BookFormViewModel
    {
        public BookDto Book { get; set; }

        public List<SelectListItem> Categories { get; set; }
        
        public List<SelectListItem> Authors { get; set; }
        
        public List<int> SelectedCategoryIds { get; set; }
        
        public List<int> SelectedAuthorIds { get; set; }
    }
}