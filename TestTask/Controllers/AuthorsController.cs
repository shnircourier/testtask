﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTask.Business.Services.Authors;

namespace TestTask.Api.Controllers
{
    public class AuthorsController : Controller
    {
        private readonly IAuthorsService _authorsService;
        
        public AuthorsController(IAuthorsService authorsService)
        {
            _authorsService = authorsService;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            var authors = await _authorsService.GetPages(page);

            return View(authors);
        }
    }
}