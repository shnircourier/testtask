﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestTask.Data.Dtos;
using TestTask.Data.Entities;

namespace TestTask.Data.Repositories.Books
{
    public class BooksRepository : IBooksRepository
    {
        private readonly DatabaseContext _dbContext;

        public BooksRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<Book>> GetBooks(int skip, int take, BooksFilterDto booksFilterDto)
        {
            var query = _dbContext.Books
                .Include(b => b.BookCategories)
                .ThenInclude(bc => bc.Category)
                .Include(b => b.AuthorBooks)
                .ThenInclude(ab => ab.Author)
                .AsQueryable();

            query = ApplyBooksFilter(query, booksFilterDto);
            
            return query
                    .OrderBy(b => b.Id)
                    .Skip(skip)
                    .Take(take)
                    .ToListAsync();
        }

        public Task<int> GetCount(BooksFilterDto booksFilterDto)
        {
            var query = _dbContext.Books.AsQueryable();

            query = ApplyBooksFilter(query, booksFilterDto);
            
            return query.CountAsync();
        }

        public Task<Book> GetById(int id)
        {
            return _dbContext.Books
                .Include(b => b.BookCategories)
                .ThenInclude(bc => bc.Category)
                .Include(b => b.AuthorBooks)
                .ThenInclude(ab => ab.Author)
                .FirstOrDefaultAsync(b => b.Id == id);
        }

        public async Task<int> Create(Book book)
        {
            var newBook = _dbContext.Books.Add(book);

            await _dbContext.SaveChangesAsync();

            return book.Id;
        }

        public async Task<int> Update(Book book)
        {
            _dbContext.Books.Update(book);
            await _dbContext.SaveChangesAsync();
            return book.Id;
        }
        
        private IQueryable<Book> ApplyBooksFilter(IQueryable<Book> query, BooksFilterDto booksFilterDto)
        {
            if (booksFilterDto is null)
            {
                return query;
            }

            var splitedFio = SplitFioBySpace(booksFilterDto.Fio)?.Select(f => f.ToLower()).ToArray();

            if (booksFilterDto.Fio is not null && splitedFio.Length == 1)
            {
                query = query.Where(b => b.AuthorBooks.Any(ab =>
                    ab.Author.Surname.ToLower().StartsWith(splitedFio[0]) ||
                    ab.Author.Name.ToLower().StartsWith(splitedFio[0]) ||
                    ab.Author.Patronymic.ToLower().StartsWith(splitedFio[0])
                    ));
            }

            if (booksFilterDto.Fio is not null && splitedFio.Length == 2)
            {
                query = query.Where(b => b.AuthorBooks.Any(ab => 
                    ((ab.Author.Name.ToLower().StartsWith(splitedFio[0]) &&
                     ab.Author.Patronymic.ToLower().StartsWith(splitedFio[1])) ||
                    (ab.Author.Surname.ToLower().StartsWith(splitedFio[0]) &&
                     ab.Author.Patronymic.ToLower().StartsWith(splitedFio[1])) ||
                    (ab.Author.Surname.ToLower().StartsWith(splitedFio[0]) &&
                     ab.Author.Name.ToLower().StartsWith(splitedFio[1]))) ||
                    ((ab.Author.Name.ToLower().StartsWith(splitedFio[1]) &&
                      ab.Author.Patronymic.ToLower().StartsWith(splitedFio[0])) ||
                     (ab.Author.Surname.ToLower().StartsWith(splitedFio[1]) &&
                      ab.Author.Patronymic.ToLower().StartsWith(splitedFio[0])) ||
                     (ab.Author.Surname.ToLower().StartsWith(splitedFio[1]) &&
                      ab.Author.Name.ToLower().StartsWith(splitedFio[0])))
                ));
            }
            
            if (booksFilterDto.Fio is not null && splitedFio.Length == 3)
            {
                query = query.Where(b => b.AuthorBooks.Any(ab =>
                    (ab.Author.Surname.ToLower().StartsWith(splitedFio[0]) ||
                     ab.Author.Surname.ToLower().StartsWith(splitedFio[1]) ||
                     ab.Author.Surname.ToLower().StartsWith(splitedFio[2])) &&
                    (ab.Author.Name.ToLower().StartsWith(splitedFio[0]) ||
                     ab.Author.Name.ToLower().StartsWith(splitedFio[1]) ||
                     ab.Author.Name.ToLower().StartsWith(splitedFio[2])) &&
                    (ab.Author.Patronymic.ToLower().StartsWith(splitedFio[0]) ||
                     ab.Author.Patronymic.ToLower().StartsWith(splitedFio[1]) ||
                     ab.Author.Patronymic.ToLower().StartsWith(splitedFio[2]))
                ));
            }

            if (booksFilterDto.BookName is not null)
            {
                query = query.Where(b => b.Title
                    .ToLower()
                    .StartsWith(booksFilterDto.BookName.ToLower()));
            }
            
            if (booksFilterDto.Category is not null)
            {
                query = query.Where(b =>
                    b.BookCategories.Any(bc =>
                        bc.Category.Id == booksFilterDto.Category));
            }

            if (booksFilterDto.WroteAt is not null)
            {
                query = query.Where(b => b.WroteAt.Year == booksFilterDto.WroteAt);
            }

            return query;
        }
                
        private string[] SplitFioBySpace(string fio)
        {
            return fio?.Split(" ", StringSplitOptions.RemoveEmptyEntries)
                .Take(3)
                .ToArray();
        }
    }
}