﻿using System;
using System.Collections.Generic;

namespace TestTask.Business.Services.Books.Dtos
{
    public class FormBookDto
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime WroteAt { get; set; }
        
        public string Isbn { get; set; }
        
        public string Text { get; set; }

        public string ImageUrl { get; set; }
        
        public List<int> Categories { get; set; }
        
        public List<int> Authors { get; set; }
    }
}