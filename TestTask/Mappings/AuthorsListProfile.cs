﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using TestTask.Business.Services.Authors.Dtos;

namespace TestTask.Api.Mappings
{
    public class AuthorsListProfile : Profile
    {
        public AuthorsListProfile()
        {
            CreateMap<AuthorDto, SelectListItem>()
                .ForMember(dest => dest.Value, opt => 
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Text, opt => 
                    opt.MapFrom(src => src.Fio));
        }
    }
}