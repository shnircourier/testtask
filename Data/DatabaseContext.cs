﻿using Microsoft.EntityFrameworkCore;
using TestTask.Data.Entities;

namespace TestTask.Data
{
    public class DatabaseContext : DbContext 
    {
        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Category> Categories { get; set; }
        
        public DbSet<AuthorBook> AuthorBooks { get; set; }
        
        public DbSet<BookCategory> BookCategories { get; set; }

        public DatabaseContext(DbContextOptions opts) : base(opts) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BookConfiguration());
            modelBuilder.ApplyConfiguration(new AuthorConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new AuthorBookConfiguration());
            modelBuilder.ApplyConfiguration(new BookCategoryConfiguration());
        }
    }
}