﻿using AutoMapper;
using TestTask.Business.Services.Categories.Dtos;
using TestTask.Data.Entities;

namespace TestTask.Business.Services.Categories.Mappings
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryDto>();
        }
    }
}