﻿using System.Threading.Tasks;
using TestTask.Business.Services.Books.Dtos;
using TestTask.Business.Services.Paginations.Dtos;

namespace TestTask.Business.Services.Books
{
    public interface IBooksService
    {
        Task<PaginationViewModel<BookDto>> GetPages(GetBooksRequest? bookParams);
        
        Task<BookDto> GetById(int id);

        Task<int> Create(FormBookDto formBookDto);

        Task Update(int id, FormBookDto formBookDto);

        public Task<BookStatisticsDto> GetBookStatistics(int id);
    }
}