﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using TestTask.Business.Services.Categories.Dtos;

namespace TestTask.Api.Mappings
{
    public class CategoriesListProfile : Profile
    {
        public CategoriesListProfile()
        {
            CreateMap<CategoryDto, SelectListItem>()
                .ForMember(dest => dest.Value, opt => 
                    opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Text, opt => 
                    opt.MapFrom(src => src.Name));
        }
    }
}