﻿using System;
using System.Collections.Generic;
using TestTask.Business.Services.Authors.Dtos;
using TestTask.Business.Services.Categories.Dtos;

namespace TestTask.Business.Services.Books.Dtos
{
    public class BookDto
    {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime WroteAt { get; set; }
        
        public string Isbn { get; set; }
        
        public string Text { get; set; }

        public string ImageUrl { get; set; }

        public List<AuthorDto> Authors { get; set; }

        public List<CategoryDto> Categories  { get; set; }
    }
}