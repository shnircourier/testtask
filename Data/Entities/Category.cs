﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestTask.Data.Entities
{
    [Table("Categories")]
    public class Category
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public List<BookCategory> BookCategories { get; set; }
    }

    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasIndex(c => c.Name).IsUnique();
            builder.Property(c => c.Name).HasMaxLength(sbyte.MaxValue);
        }
    }
}