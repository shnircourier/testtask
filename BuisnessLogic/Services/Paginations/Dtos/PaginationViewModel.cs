﻿using System.Collections.Generic;

namespace TestTask.Business.Services.Paginations.Dtos
{
    public class PaginationViewModel<T>
    {
        public PaginationDto Pagination { get; set; }

        public List<T> Items { get; set; }
    }
}