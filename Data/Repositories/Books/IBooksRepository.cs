﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestTask.Data.Dtos;
using TestTask.Data.Entities;

namespace TestTask.Data.Repositories.Books
{
    public interface IBooksRepository
    {
        Task<List<Book>> GetBooks(int skip, int take, BooksFilterDto booksFilterDto);

        Task<int> GetCount(BooksFilterDto booksFilterDto);

        Task<Book> GetById(int id);

        Task<int> Create(Book book);

        Task<int> Update(Book book);
    }
}