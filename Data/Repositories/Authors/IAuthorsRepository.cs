﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestTask.Data.Entities;

namespace TestTask.Data.Repositories.Authors
{
    public interface IAuthorsRepository
    {
        Task<List<Author>> GetAuthors(int skip, int take);

        Task<List<Author>> GetAll();

        Task<int> GetCount();

        Task<Author> GetById(int id);
    }
}