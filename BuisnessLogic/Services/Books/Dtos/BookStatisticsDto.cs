﻿namespace TestTask.Business.Services.Books.Dtos
{
    public class BookStatisticsDto
    {
        public int TextLenght { get; set; }

        public int AmountOfWords { get; set; }

        public int AmountOfUniqueWords { get; set; }

        public int SentenceAverageLenght { get; set; }

        public int WordAverageLenght { get; set; }
    }
}