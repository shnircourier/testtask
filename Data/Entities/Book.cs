﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestTask.Data.Entities
{
    [Table("Books")]
    public class Book
    {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime WroteAt { get; set; }
        
        public string Isbn { get; set; }
        
        public string Text { get; set; }

        public string ImageUrl { get; set; }

        public List<AuthorBook> AuthorBooks { get; set; }

        public List<BookCategory> BookCategories  { get; set; }
    }

    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasIndex(b => b.Isbn).IsUnique();
            builder.Property(b => b.Isbn).HasMaxLength(sbyte.MaxValue);
            builder.Property(b => b.Title).HasMaxLength(sbyte.MaxValue);
            builder.Property(b => b.Description).HasMaxLength(byte.MaxValue);
            builder.Property(b => b.ImageUrl).HasMaxLength(byte.MaxValue);
        }
    } 
}