﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TestTask.Api.Models;
using TestTask.Business.Services.Authors;
using TestTask.Business.Services.Books;
using TestTask.Business.Services.Books.Dtos;
using TestTask.Business.Services.Categories;

namespace TestTask.Api.Controllers
{
    public class BooksController : Controller
    {
        private readonly IAuthorsService _authorsService;
        private readonly IBooksService _booksService;
        private readonly ICategoriesService _categoryService;
        private readonly IMapper _mapper;

        public BooksController(
            ICategoriesService categoriesService,
            IAuthorsService authorsService,
            IBooksService booksService,
            IMapper mapper)
        {
            _categoryService = categoriesService;
            _authorsService = authorsService;
            _booksService = booksService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index([FromQuery] GetBooksRequest request)
        {
            var books = await _booksService.GetPages(request);
            var categories = await _categoryService.GetAll();

            return View(new BooksPageViewModel
            {
                Books = books,
                Categories = categories
            });
        }

        [Route("Books/{id}")]
        public async Task<IActionResult> Show(int id)
        {
            var book = await _booksService.GetById(id);
            var bookStatistics = await _booksService.GetBookStatistics(id);

            return View(new BookViewModel {Book = book, BookStatistics = bookStatistics});
        }

        [Route("Books/Create")]
        public async Task<IActionResult> Create()
        {
            var categories = await _categoryService.GetAll();
            var authors = await _authorsService.GetAll();

            var model = new BookFormViewModel
            {
                Categories = _mapper.Map<List<SelectListItem>>(categories),
                Authors = _mapper.Map<List<SelectListItem>>(authors)
            };

            return View(model);
        }

        [Route("Books/{id}/Edit")]
        public async Task<IActionResult> Edit([FromRoute] int id)
        {
            var categories = await _categoryService.GetAll();
            var authors = await _authorsService.GetAll();
            var book = await _booksService.GetById(id);

            var model = new BookFormViewModel
            {
                Categories = _mapper.Map<List<SelectListItem>>(categories),
                Authors = _mapper.Map<List<SelectListItem>>(authors),
                Book = book,
                SelectedCategoryIds = book.Categories.Select(c => c.Id).ToList(),
                SelectedAuthorIds = book.Authors.Select(a => a.Id).ToList()
            };

            return View(model);
        }

        [HttpPost]
        [Route("Books")]
        public async Task<IActionResult> Create([FromBody] FormBookDto book)
        {
            var bookId = await _booksService.Create(book);

            return RedirectToAction(nameof(Show), new {id = bookId});
        }

        // HttpPost необходим для формы
        [HttpPost, HttpPut]
        [Route("Books/{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] FormBookDto book)
        {
            await _booksService.Update(id, book);

            return RedirectToAction(nameof(Show), new {id = id});
        }
    }
}