﻿namespace TestTask.Business.Services.Paginations
{
    public class PaginationService : IPaginationService
    {
        public int GetNextPage(int page, int pageSize)
        {
            return (page - 1) * pageSize;
        }
    }
}