﻿using AutoMapper;
using TestTask.Business.Services.Authors.Dtos;
using TestTask.Data.Entities;

namespace TestTask.Business.Services.Authors.Mappings
{
    public class AuthorsProfile : Profile
    {
        public AuthorsProfile()
        {
            CreateMap<Author, AuthorDto>()
                .ForMember(dest => dest.Fio,
                    opt =>
                        opt.MapFrom(src => $"{src.Surname} {src.Name} {src.Patronymic}"));
        }
    }
}