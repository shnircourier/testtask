﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTask.Business.Services.Categories;
using TestTask.Business.Services.Categories.Dtos;

namespace TestTask.Api.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICategoriesService _categoriesService;

        public CategoriesController(ICategoriesService categoriesService)
        {
            _categoriesService = categoriesService;
        }

        public async Task<IActionResult> Index()
        {
            var categories = await _categoriesService.GetAll();

            return View(new CategoriesViewModel{Categories = categories});
        }
    }
}