﻿using Microsoft.Extensions.DependencyInjection;
using TestTask.Data.Repositories.Authors;
using TestTask.Data.Repositories.Books;
using TestTask.Data.Repositories.Categories;

namespace TestTask.Data.Extensions
{
    public static class DataStartupExtension
    {
        public static IServiceCollection AddDataServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IAuthorsRepository, AuthorsRepository>();
            serviceCollection.AddScoped<IBooksRepository, BooksRepository>();
            serviceCollection.AddScoped<ICategoriesRepository, CategoriesRepository>();

            return serviceCollection;
        }
    }
}