﻿using System;
using System.Collections.Generic;
using System.Linq;
using EFCore.BulkExtensions;
using TestTask.Data.Entities;

namespace TestTask.Data
{
    public class Seeder
    {
        public static void Populate(DatabaseContext context)
        {
            if(!context.Categories.Any()) context.BulkInsert(PopulateCategories());
            if(!context.Authors.Any()) context.BulkInsert(PopulateAuthors());
            if(!context.Books.Any()) context.BulkInsert(PopulateBooks());
            if(!context.BookCategories.Any()) context.BulkInsert(PopulateBookCategories());
            if(!context.AuthorBooks.Any()) context.BulkInsert(PopulateAuthorBooks());
        }

        private static List<Category> PopulateCategories()
        {
            var categories = new List<Category>();

            for (var i = 1; i <= 100; i++)
            {
                categories.Add(new Category
                {
                    Name = $"Жанр #{i}"
                });
            }

            return categories;
        }

        private static List<Author> PopulateAuthors()
        {
            var authors = new List<Author>();

            for (var i = 1; i <= 10_000; i++)
            {
                authors.Add(new Author
                {
                    Name = $"Имя{i}",
                    Patronymic = $"Отчество{i}",
                    Surname = $"Фамилия{i}",
                    BirthDate = DateTime.Parse("2021-10-20").Subtract(TimeSpan.FromDays(i)),
                    PhotoUrl = "https://d33wubrfki0l68.cloudfront.net/b4b0f1e65367c7ae5c5b01d4036b8113a815dfbc/ea3e7/images/uploads/bmj_lovecraft.png"
                });
            }
            
            return authors;
        }

        private static List<Book> PopulateBooks()
        {
            var books = new List<Book>();

            for (var i = 1; i <= 100_000; i++)
            {
                books.Add(new Book
                {
                    Isbn = $"1111-111-{i}",
                    Title = $"Название книги {i}",
                    Description = $"Описание книги {i}",
                    Text = $"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.{i}",
                    ImageUrl = "https://cdnimg.rg.ru/img/content/170/98/06/kniga1000_default_d_850.jpg",
                    WroteAt = DateTime.Parse("2021-10-20").Subtract(TimeSpan.FromDays(i))

                });
            }
            
            return books;
        }

        private static List<BookCategory> PopulateBookCategories()
        {
            var bookCategories = new List<BookCategory>();

            var rand = new Random();
            
            for (var i = 1; i <= 100_000; i++)
            {
                bookCategories.Add(new BookCategory
                {
                    CategoryId = rand.Next(1,100),
                    BookId = i
                });
            }
            
            return bookCategories;
        }

        private static List<AuthorBook> PopulateAuthorBooks()
        {
            var authorBooks = new List<AuthorBook>();

            var rand = new Random();
            
            for (var i = 1; i <= 100_000; i++)
            {
                authorBooks.Add(new AuthorBook
                {
                    AuthorId = rand.Next(1, 10_000),
                    BookId = i
                });
            }

            return authorBooks;
        }
    }
}