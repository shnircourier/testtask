﻿using System.Linq;
using AutoMapper;
using TestTask.Business.Services.Books.Dtos;
using TestTask.Data.Dtos;
using TestTask.Data.Entities;

namespace TestTask.Business.Services.Books.Mappings
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookDto>()
                .ForMember(dest => dest.Authors,
                    opt => opt.MapFrom(src =>
                        src.AuthorBooks.Select(ab => ab.Author).ToList()))
                .ForMember(dest => dest.Categories,
                    opt => opt.MapFrom(src =>
                        src.BookCategories.Select(bc => bc.Category).ToList()));
            
            CreateMap<GetBooksRequest, BooksFilterDto>();
        }
    }
}