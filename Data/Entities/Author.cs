﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestTask.Data.Entities
{
    [Table("Authors")]
    public class Author
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public string Surname { get; set; }
        
        public string Patronymic { get; set; }

        public DateTime BirthDate { get; set; }
        
        public string PhotoUrl { get; set; }

        public List<AuthorBook> AuthorBooks { get; set; }
    }

    public class AuthorConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.Property(a => a.Name).HasMaxLength(byte.MaxValue);
            builder.Property(a => a.Surname).HasMaxLength(byte.MaxValue);
            builder.Property(a => a.Patronymic).HasMaxLength(byte.MaxValue);
            builder.Property(a => a.PhotoUrl).HasMaxLength(byte.MaxValue);
        }
    }
}