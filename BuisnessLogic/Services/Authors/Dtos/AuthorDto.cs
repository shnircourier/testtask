﻿using System;

namespace TestTask.Business.Services.Authors.Dtos
{
    public class AuthorDto
    {
        public int Id { get; set; }
        
        public string Fio { get; set; }
        
        public DateTime BirthDate { get; set; }
        
        public string PhotoUrl { get; set; }
    }
}