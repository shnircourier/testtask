﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestTask.Data.Entities;

namespace TestTask.Data.Repositories.Categories
{
    public class CategoriesRepository : ICategoriesRepository
    {
        private readonly DatabaseContext _dbContext;

        public CategoriesRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<Category>> GetAll()
        {
            return _dbContext.Categories.ToListAsync();
        }

        public Task<Category> GetById(int id)
        {
            return _dbContext.Categories.FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}