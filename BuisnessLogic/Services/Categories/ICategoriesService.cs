﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestTask.Business.Services.Categories.Dtos;

namespace TestTask.Business.Services.Categories
{
    public interface ICategoriesService
    {
        Task<List<CategoryDto>> GetAll();
    }
}