﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace TestTask.Data
{
    public static class MigrationManager
    {
        public static IHost MigrateDatabaseWithSeeds(this IHost host)
        {
            using var scoped = host.Services.CreateScope();
            using var context = scoped.ServiceProvider.GetRequiredService<DatabaseContext>();
            try
            {
                context.Database.Migrate();
                Seeder.Populate(context);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return host;
        }
    }
}