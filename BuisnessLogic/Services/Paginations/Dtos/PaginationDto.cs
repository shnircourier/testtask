﻿namespace TestTask.Business.Services.Paginations.Dtos
{
    public class PaginationDto
    {
        public IQueryParams QueryParams { get; set; }

        public PageViewModel Pagination { get; set; }
    }

    public interface IQueryParams
    {
    }
}