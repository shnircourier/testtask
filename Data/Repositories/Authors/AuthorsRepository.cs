﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestTask.Data.Entities;

namespace TestTask.Data.Repositories.Authors
{
    public class AuthorsRepository : IAuthorsRepository
    {
        private readonly DatabaseContext _dbContext;

        public AuthorsRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<Author>> GetAuthors(int skip, int take)
        {
            return _dbContext.Authors
                .OrderBy(a => a.Id)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }

        public Task<List<Author>> GetAll()
        {
            return _dbContext.Authors.ToListAsync();
        }

        public Task<int> GetCount()
        {
            return _dbContext.Authors.CountAsync();
        }

        public Task<Author> GetById(int id)
        {
            return _dbContext.Authors.FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}