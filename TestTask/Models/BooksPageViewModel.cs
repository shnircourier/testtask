﻿using System.Collections.Generic;
using TestTask.Business.Services.Books.Dtos;
using TestTask.Business.Services.Categories.Dtos;
using TestTask.Business.Services.Paginations.Dtos;

namespace TestTask.Api.Models
{
    public class BooksPageViewModel
    {
        public List<CategoryDto> Categories { get; set; }

        public PaginationViewModel<BookDto> Books { get; set; }
    }
}