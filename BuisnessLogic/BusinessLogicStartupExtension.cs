﻿using Microsoft.Extensions.DependencyInjection;
using TestTask.Business.Services.Authors;
using TestTask.Business.Services.Authors.Mappings;
using TestTask.Business.Services.Books;
using TestTask.Business.Services.Books.Mappings;
using TestTask.Business.Services.Categories;
using TestTask.Business.Services.Categories.Mappings;
using TestTask.Business.Services.Paginations;

namespace TestTask.Business
{
    public static class BusinessLogicStartupExtension
    {
        public static IServiceCollection AddBusinessLogicServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IBooksService, BooksService>();
            serviceCollection.AddScoped<IAuthorsService, AuthorsService>();
            serviceCollection.AddScoped<ICategoriesService, CategoriesService>();
            serviceCollection.AddScoped<IPaginationService, PaginationService>();
            serviceCollection.AddAutoMapper(
                typeof(BookProfile),
                typeof(CategoryProfile),
                typeof(AuthorsProfile));
            
            return serviceCollection;
        }
    }
}