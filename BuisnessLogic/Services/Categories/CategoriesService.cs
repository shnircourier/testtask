﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using TestTask.Business.Services.Categories.Dtos;
using TestTask.Data.Repositories.Categories;

namespace TestTask.Business.Services.Categories
{
    public class CategoriesService : ICategoriesService
    {
        private readonly ICategoriesRepository _categoriesRepository;
        private readonly IMapper _mapper;

        public CategoriesService(
            ICategoriesRepository categoriesRepository,
            IMapper mapper)
        {
            _categoriesRepository = categoriesRepository;
            _mapper = mapper;
        }

        public async Task<List<CategoryDto>> GetAll()
        {
            var categories = await _categoriesRepository.GetAll();

            return _mapper.Map<List<CategoryDto>>(categories);
        }
    }
}