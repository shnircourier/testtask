﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TestTask.Business.Services.Books.Dtos;
using TestTask.Business.Services.Paginations;
using TestTask.Business.Services.Paginations.Dtos;
using TestTask.Data.Dtos;
using TestTask.Data.Entities;
using TestTask.Data.Repositories.Books;

namespace TestTask.Business.Services.Books
{
    public class BooksService : IBooksService
    {
        private readonly IBooksRepository _bookRepository;
        private readonly IMapper _mapper;
        private readonly IPaginationService _paginationService;

        public BooksService(
            IBooksRepository booksRepository,
            IPaginationService paginationService,
            IMapper mapper
        )
        {
            _bookRepository = booksRepository;
            _paginationService = paginationService;
            _mapper = mapper;
        }

        public async Task<PaginationViewModel<BookDto>> GetPages(GetBooksRequest bookParams)
        {
            const int pageSize = 9;

            var skip = _paginationService.GetNextPage(bookParams.Page, pageSize);
            var booksFilterDto = _mapper.Map<BooksFilterDto>(bookParams);
            var books = await _bookRepository.GetBooks(skip, pageSize, booksFilterDto);
            var count = await _bookRepository.GetCount(booksFilterDto);

            return new PaginationViewModel<BookDto>
            {
                Pagination = new PaginationDto
                {
                    Pagination = new PageViewModel(count, bookParams.Page, pageSize),
                    QueryParams = bookParams
                },
                Items = _mapper.Map<List<BookDto>>(books)
            };
        }

        public async Task<BookDto> GetById(int id)
        {
            var book = await _bookRepository.GetById(id);

            return _mapper.Map<BookDto>(book);
        }

        public async Task<int> Create(FormBookDto formBookDto)
        {
            var book = new Book
            {
                Text = formBookDto.Text,
                Description = formBookDto.Description,
                Isbn = formBookDto.Isbn,
                Title = formBookDto.Title,
                ImageUrl = formBookDto.ImageUrl,
                WroteAt = formBookDto.WroteAt
            };

            book.AuthorBooks.AddRange(formBookDto.Authors.Select(a => new AuthorBook
            {
                Book = book,
                AuthorId = a
            }));

            book.BookCategories.AddRange(formBookDto.Categories.Select(c => new BookCategory
            {
                Book = book,
                CategoryId = c
            }));

            return await _bookRepository.Create(book);
        }

        public async Task Update(int id, FormBookDto formBookDto)
        {
            var book = await _bookRepository.GetById(id);

            book.Description = formBookDto.Description;
            book.Isbn = formBookDto.Isbn;
            book.Text = formBookDto.Text;
            book.Title = formBookDto.Title;
            book.ImageUrl = formBookDto.ImageUrl;
            book.WroteAt = formBookDto.WroteAt;

            book.AuthorBooks.Clear();
            book.BookCategories.Clear();

            book.AuthorBooks.AddRange(formBookDto.Authors.Select(a => new AuthorBook
            {
                AuthorId = a,
                BookId = id
            }));

            book.BookCategories.AddRange(formBookDto.Categories.Select(c => new BookCategory
            {
                CategoryId = c,
                BookId = id
            }));

            await _bookRepository.Update(book);
        }

        public async Task<BookStatisticsDto> GetBookStatistics(int id)
        {
            var sentenceSeparator = new[] {'.', '?', '!'};
            var wordsSeparator = new[] {' ', '.', ',', '!', '?'};
            var book = await GetById(id);

            return new BookStatisticsDto
            {
                TextLenght = book.Text.Length,
                AmountOfWords = book.Text
                    .Split(wordsSeparator, StringSplitOptions.RemoveEmptyEntries)
                    .Length,
                AmountOfUniqueWords = book.Text
                    .ToLower()
                    .Split(wordsSeparator, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .Count(),
                SentenceAverageLenght = (int) book.Text
                    .Split(sentenceSeparator, StringSplitOptions.TrimEntries)
                    .Average(x => x.Length),
                WordAverageLenght = (int) book.Text
                    .Split(wordsSeparator, StringSplitOptions.RemoveEmptyEntries)
                    .Average(x => x.Length)
            };
        }
    }
}